class CreateLangs < ActiveRecord::Migration
  def change
    create_table :langs do |t|
      t.string :name
      t.string :code, limit: 2
      t.string :icon_path

      t.timestamps null: false
    end
    add_index :langs, :code, unique: true
    add_index :langs, :icon_path, unique: true
  end
end
