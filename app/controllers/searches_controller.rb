class SearchesController < ApplicationController
  def show
    if params[:query]
      @results = Document.search(params[:query], page: params[:page], per_page: 20, highlight: true, fields: [:title, :description])
      @exact_match = Document.title_is(params[:query])

      redirect_to @exact_match if @results == [@exact_match]
    end
  end
end
