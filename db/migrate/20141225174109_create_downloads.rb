class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.attachment :download
      t.references :document, index: true
      t.integer :download_counter, null: false, default: 0

      t.timestamps null: false
    end
    add_foreign_key :downloads, :documents
  end
end
