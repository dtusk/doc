module SessionsHelper
  def sign_in (account, remember = "0")
    session_token = Account.new_session_token
    if remember == "1"
      cookies.permanent.signed[:session_token] = session_token
    else
      cookies.signed[:session_token] = session_token
    end

    account.update_attribute(:session_token, Account.digest(session_token))
    self.current_account = account
  end

  def sign_out
    current_account.update_attribute(:session_token, Account.new_session_token)
    cookies.delete(:session_token)
    self.current_account = nil
  end

  def signed_in?
    !current_account.nil?
  end

  def current_account=(account)
    @current_account = account
  end

  def current_account
    session_token = Account.digest(cookies.signed[:session_token])
    @current_account ||= Account.find_by(session_token: session_token)
  end
end
