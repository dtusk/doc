class Token < ActiveRecord::Base
  belongs_to :account

  def self.create_confirmation_token account
    begin
      token = self.random_string(30)
    end while self.exists?(token: token)
    create(account: account, token: token, confirmation: true)
    token
  end

  def self.create_resetting_token account
    begin
      token = self.random_string(30)
    end while self.exists?(token: token)
    create(account: account, token: token, reset: true)
    token
  end

  def self.random_string(length, include_uppercase = true, include_lowercase = true, include_numbers = false)
    o = []
    o.push ('a'..'z') if include_uppercase
    o.push ('A'..'Z') if include_lowercase
    
    o.push (0..9) if include_numbers
    o = o.map { |i| i.to_a }.flatten
    (0...length).map { o[rand(o.length)] }.join
  end
end
