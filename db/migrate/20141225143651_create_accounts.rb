class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :user_name
      t.string :email, null: false, default: ""
      t.string :password_digest, null: false, default: ""
      t.string :slug
      t.string :session_token
      t.integer :warnings, null: false, default: 0, limit: 1
      t.integer :rank, null: false, default: 0, limit: 1 
      t.integer :sign_in_count, null: false, default: 0
      t.boolean :banned, null: false, default: false
      t.boolean :activated, null: false, default: false
      t.timestamps null: false
    end
    add_index :accounts, :user_name, unique: true
    add_index :accounts, :email, unique: true
    add_index :accounts, :slug, unique: true
  end
end
