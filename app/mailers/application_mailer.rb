class ApplicationMailer < ActionMailer::Base
  default from: "dashvosz@gmail.com"
  layout 'mailer'
end
