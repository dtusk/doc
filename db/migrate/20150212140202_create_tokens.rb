class CreateTokens < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.references :account, index: true
      t.string :token
      t.boolean :reset, null: false, default: false
      t.boolean :confirmation, null: false, default: false

      t.timestamps null: false
    end
    add_foreign_key :tokens, :accounts
  end
end
