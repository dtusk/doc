//= require jquery
//= require jquery_ujs
//= require modernizr.min
//= require foundation.min
//= require cookies

$(document).foundation();

$(document).ready(function() {
  if (docCookies.hasItem('accepted_cookies')) {
    $('.about_cookies').remove();
  } else {
    $('.about_cookies').removeClass('hidden');

    $('a.close_cookies_button').click(function() {
      docCookies.setItem('accepted_cookies', 'true', Infinity);
      $('.about_cookies').remove();
    });
  }

});
