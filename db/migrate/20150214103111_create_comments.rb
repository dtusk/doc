class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :document, index: true
      t.references :account, index: true
      t.text :body
      t.inet :sender_ip
      t.string :sender_hostname
      t.references :response, index: true
      t.timestamps null: false
    end
    add_foreign_key :comments, :documents
    add_foreign_key :comments, :accounts
    add_foreign_key :comments, :comments, name: 'response_id', column: 'response_id'
  end
end
