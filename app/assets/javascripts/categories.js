//= require jstree.min

$("#categoryTree").jstree().delegate(".jstree-open>a", "click.jstree", function(event){
      $.jstree._reference(this).close_node(this,false,false);

}).delegate(".jstree-closed>a", "click.jstree", function(event){
      $.jstree._reference(this).open_node(this,false,false);

});
