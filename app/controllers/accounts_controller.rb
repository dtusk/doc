class AccountsController < ApplicationController
  def show
    @account = Account.friendly.find(params[:id])
    @documents = Document.where(uploader: @account).order(:created_at).limit(5)
    @comments = Comment.where(account: @account).order(:created_at).limit(5)
  end
end
