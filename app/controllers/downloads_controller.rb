class DownloadsController < ApplicationController

  before_filter :auth_account

  def show
    @download = Download.find(params[:id])
    @download.download_counter += 1
    @download.save

    send_data File.read(@download.download.path),
          :type => "#{@download.download_content_type}; charset=UTF-8;",
              :disposition => "attachment; filename=#{@download.download_file_name}"
  end
end
