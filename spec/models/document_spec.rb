require 'rails_helper'

RSpec.describe Document, :type => :model do

  before { @document = FactoryGirl.create(:document) }

  subject { @document }

  it { should respond_to(:title) }
  it { should respond_to(:slug) }
  it { should respond_to(:description) }
  it { should respond_to(:tags) }
  it { should respond_to(:image_url) }
  it { should respond_to(:category) }
  it { should respond_to(:uploader) }
  it { should respond_to(:downloads) }
  it { should respond_to(:lang) }
  it { should respond_to(:viewed) }
  it { should respond_to(:rating) }
  it { should be_valid }

end
