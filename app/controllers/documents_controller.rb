class DocumentsController < ApplicationController


  before_filter :auth_account

  def show
    @document = Document.friendly.find(params[:id])
    @document.views += 1
    @document.save
  end
  
end
