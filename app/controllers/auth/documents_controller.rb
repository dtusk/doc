class Auth::DocumentsController < ApplicationController


  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_action :auth_account

  def index
    @documents = Document.where(uploader: current_account).page(params[:page])

  end

  def show

  end

  def new
    @categories = Category.all
  end

  def create
    @document = Document.new(document_params)
    @document.uploader = current_account
    respond_to do |f|
      if @document.save
        f.html { redirect_to auth_documents_new_step_2_path(@document) }
      else
        @download.destroy
        f.html { render :new }
      end
    end
  end

  def step_2
    redirect_to root_path unless Document.exists?(slug: params[:id])
  end


  def update
    unless is_owner?
      flash.now[:error] = I18n.t('not_your_document')
      redirect_to auth_documents_path
    end

    respond_to do |f|
      if @document.update_attributes(title: document_params[:title],
                                     tags: document_params[:tags],
                                     category_id: document_params[:category_id],
                                     description: document_params[:description])
        f.html { redirect_to auth_documents_path, success: I18n.t('document_was_successfully_updated') }
        f.json { head :no_content }
      else
        f.html { render :new }
        f.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end


  def edit
    unless is_owner?
      flash.now[:error] = I18n.t('not_your_document')
      redirect_to auth_documents_path
    end
  end

  def destroy
    @document.destroy

    respond_to do |f|
      f.html { redirect_to auth_documents_path }
      f.json { head :no_content }
    end

  end

  private
    def document_params
      params.require(:document).permit(:title, :description, :tags, :category_id)
    end

    def set_document
      @document = Document.where(slug: params[:id], uploader: current_account).limit(1).take
    end

    def is_owner?
      @document.uploader == current_account
    end

end
