require 'sidekiq/web'


Rails.application.routes.draw do


  mount Sidekiq::Web, at: '/sidekiq'
  resources :documents, only: :show, path: 'doc' do
    get 'download/:id' => 'downloads#show', as: :download
  end
  get 'search/autocomplete' => 'searches#autocomplete'
  get 'search' => 'searches#show'
  get 'account/:id' => 'accounts#show', as: :account
  post 'comments/new' => 'comments#new'

  root 'home#index'

  namespace :auth do
    scope :accounts do
      root 'accounts#index'
      get 'log_in' => 'accounts#log_in', as: 'log_in_session'
      post 'log_in' => 'accounts#do_log_in'
      delete 'log_out' => 'accounts#log_out', as: 'log_out_session'
      get 'register' => 'accounts#register', as: 'register_account'
      post 'register' => 'accounts#do_register'
      get 'activate/:id' => 'accounts#activate', as: 'activate_account'
      get 'forgot' => 'accounts#forgot', as: 'forgot_password_account'
      post 'forgot' => 'accounts#do_forgot'
      get 'reset/:id' => 'accounts#reset', as: 'reset_password_account'
      post 'reset/:id' => 'accounts#do_reset'
    end
    namespace :admin do
      root 'admin#index'
      resources :documents
      resources :categories
    end

    namespace :staff do
      root 'staff#index'
      resources :accounts do
        get 'ban' => 'accounts#ban_account'
      end
    end
    resources :documents, path_names: {new: 'new/step-1'} do
      delete 'downloads/:id' => 'downloads#destroy', as: :download
    end
    get 'documents/new/:id/step-2' => 'documents#step_2', as: :documents_new_step_2
    post 'downloads' => 'downloads#create'

  end

end
