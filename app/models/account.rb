class Account < ActiveRecord::Base
  extend FriendlyId
  has_secure_password

  friendly_id :user_name, use: [:slugged, :finders]

  validates :email, presence: true, uniqueness: true
  validates :user_name, presence: true, uniqueness: true

  has_many :documents, foreign_key: 'uploader_id'
  has_many :comments

  before_save {
    self.email = email.downcase
  }

  def to_s
    user_name || email
  end

  def admin?
    rank == 4
  end

  def premium?
    rank == 2
  end

  def staff?
    rank >= 3
  end

  def self.new_session_token
    SecureRandom.urlsafe_base64
  end 

  def self.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def to_param
    user_name.parameterize
  end


  private
    def create_session_token
      self.session_token = self.digest(self.new_session_token)
    end

end
