FactoryGirl.define do
  factory :document do
    title { Faker::Lorem.sentence }
    slug { Faker::Internet.slug }
    description { Faker::Lorem.sentences }
    tags { Faker::Internet.slug(Faker::Lorem.sentence, ', ') }
    category
    association :uploader, factory: :account
    lang
    downloads
    viewed { Faker::Number.digit }
    rating { Faker::Number.between(1,5) }
  end

end
