class Comment < ActiveRecord::Base
  belongs_to :document
  belongs_to :account

  belongs_to :response

  validates :body, presence: true

end
