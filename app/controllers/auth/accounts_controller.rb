class Auth::AccountsController < ApplicationController

  before_filter :auth_account, only: [:index, :log_out]


  def index
    @documents_count = Document.where(uploader: current_account).count
  end

  def log_in

  end

  def do_log_in
    account = Account.find_by(email: params[:session][:email].downcase)
    if account && account.authenticate(params[:session][:password])
      if account.banned?
        redirect_to root_path, error: I18n.t('your_account_has_been_banned')
      elsif !account.activated?
        redirect_to root_path, error: I18n.t('your_account_has_not_been_activated')
      else
        AccountSignUpCountingJob.perform_later(account.id)
        sign_in account, params[:session][:remember_me]
        redirect_to root_path
      end
    else
      flash.now[:error] = I18n.t('invalid_email_password_combination')
      render :log_in
    end
  end

  def log_out
    sign_out
    redirect_to root_path, notice: I18n.t('you_have_been_successfully_logged_out')
  end

  def register
  end

  def do_register
    @account = Account.new(user_name: account_params[:user_name],
                             email: account_params[:email],
                             password: account_params[:password],
                             password_confirmation: account_params[:confirm_password])

    respond_to do |f|
      if @account.save
        confirmation_token = Token.create_confirmation_token(@account)
        RegisterMailer.activate_account(@account, confirmation_token).deliver_now
        f.html { redirect_to root_path, notice: I18n.t(:account_has_been_created_check_mailbox_for_confirmation) }
      else
        f.html { render :register }
      end
    end

  end

  def activate
    confirmation_token_params = params[:id]
    if confirmation_token_params
      if (confirm = Token.find_by(token: confirmation_token_params, confirmation: true))
        confirm.account.activated = true
        confirm.account.save
        confirm.destroy
        sign_in confirm.account
        redirect_to root_path
      else
        redirect_to root_path, notice: I18n.t(:invalid_token)
      end
    end
  end

  def forgot

  end

  def do_forgot
    email_params = params[:email][:email]
    if (account = Account.find_by(email: email_params))
      token = Token.create_resetting_token(account)
      TokensCleanupJob.set(wait: 5.minutes).perform_later(token)
      RegisterMailer.forgot_password(account, token).deliver_now
      redirect_to root_path, notice: I18n.t('we_send_you_an_email_with_a_reset_password_link')
    else
      flash[:error] = I18n.t(:we_do_not_have_such_email_in_our_database)
      render :forgot
    end

  end

  def reset
    reset_token_params = params[:id]
    redirect_to root_path, error: I18n.t(:this_token_does_not_exist) unless Token.exists?(token: reset_token_params, reset: true)
  end

  def do_reset
    reset_token_params = params[:id]
    if (reset_account = Token.find_by(token: reset_token_params, reset: true))
      if (reset_account.account.update_attributes(password: params[:passwords][:password],
                                                  password_confirmation: params[:passwords][:password_confirmation]))
        flash.now[:success] = I18n.t(:you_have_successfully_changed_your_password_now_you_are_able_to_login)
        reset_account.destroy
      end
      redirect_to root_path
    end
  end

  private
    def account_params
      params.require(:account).permit(:user_name, :email, :password, :confirm_password)
    end

end
