class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper


  before_filter :set_locale

   def auth_admin_account
     if auth_account
       unless current_account.admin?
         render status: 403, layout: false
       end
     end
   end

   def auth_staff_account
     if auth_account
       render status: 403, layout: false unless current_account.staff?
     end
   end

   def auth_account
     redirect_to auth_log_in_session_path unless signed_in?
   end


  private
    def set_locale
      if cookies[:locale]
        I18n.locale = cookies[:locale]
      else
        I18n.locale = cookies[:locale] = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      end
    end
end
