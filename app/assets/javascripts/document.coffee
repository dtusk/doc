$('#document_title').focusout () ->
  document_title = $('#document_title').val()
  document_title = document_title.replace(/[^a-zA-Z0-9\- ]/g, '').replace(/ /g, ',')
  document_title = document_title.toLowerCase()
  $('#document_tags').val(document_title)
  return

jQuery ->
  $('#new_download').fileupload
    type: 'script'
    add: (e, data) ->
      data.context = $(tmpl('template-upload', data.files[0]))
      $('#new_download').append(data.context)
      data.submit()
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.meter').css('width', progress + '%')
