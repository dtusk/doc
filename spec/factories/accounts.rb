FactoryGirl.define do
  factory :account do
    user_name { Faker::Internet.user_name }
    slug { Faker::Internet.slug }
    warnings { Faker::Number.digit }
  end

end
