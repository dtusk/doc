FactoryGirl.define do
  factory :download do
    title { Faker::Lorem.sentence }
    download nil
    document
    download_counter { Faker::Number.number(10) }
  end

end
