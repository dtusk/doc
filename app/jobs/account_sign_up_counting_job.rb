class AccountSignUpCountingJob < ActiveJob::Base
  queue_as :default

  def perform(account_id)
    account = Account.find(account_id)
    account.sign_in_count += 1
    account.save
  end
end
