class Category < ActiveRecord::Base
  extend FriendlyId
  belongs_to :category_parent, class_name: 'Category'

  translates :name
  has_many :documents

  friendly_id :name, use: [:slugged, :finders]
  has_many :category_children, class_name: 'Category',
    foreign_key: 'category_parent_id'

  def to_s
    name
  end
end
