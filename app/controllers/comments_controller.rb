class CommentsController < ApplicationController 

  before_filter :auth_account

  def new
    comment = Comment.new(comment_params)
    comment.account = current_account
    comment.sender_ip = request.remote_ip
    comment.sender_hostname = Resolv.getname(comment.sender_ip)

    if comment.save
      flash.now[:success] = I18n.t('comment_successfully_created')
    else
      flash.now[:error] = I18n.t('error_while_proceeding_save_comment')
    end
    redirect_to comment.document
  end

  private
    def comment_params
      params.require(:comment).permit(:body, :document_id)
    end
end
