require 'rails_helper'

RSpec.describe Category, :type => :model do

  before { @category = FactoryGirl.create(:category) }

  subject { @category }

  it { should respond_to(:name) }
  it { should respond_to(:slug) }
  it { should respond_to(:category_parent) }
  it { should respond_to(:category_children) }
  it { should be_valid }

end
