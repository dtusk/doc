module CategoriesHelper

  def generate_tree
    @parent_categories = Category.where(category_parent: nil)
    nesting_tree(@parent_categories)
  end

  private
    def nesting_tree leaves
      render = "<ul>"

      leaves.each do |leaf|
        render += "<li>" + leaf.name
        render += nesting_tree(leaf.category_children) unless leaf.category_children.empty?
        render += "</li>"
      end

      return render + "</ul>"
    end
end
