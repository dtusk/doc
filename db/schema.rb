# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150214103111) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "user_name"
    t.string   "email",                     default: "",    null: false
    t.string   "password_digest",           default: "",    null: false
    t.string   "slug"
    t.string   "session_token"
    t.integer  "warnings",        limit: 2, default: 0,     null: false
    t.integer  "rank",            limit: 2, default: 0,     null: false
    t.integer  "sign_in_count",             default: 0,     null: false
    t.boolean  "banned",                    default: false, null: false
    t.boolean  "activated",                 default: false, null: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "accounts", ["email"], name: "index_accounts_on_email", unique: true, using: :btree
  add_index "accounts", ["slug"], name: "index_accounts_on_slug", unique: true, using: :btree
  add_index "accounts", ["user_name"], name: "index_accounts_on_user_name", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string  "name"
    t.string  "slug"
    t.integer "category_parent_id"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "category_translations", force: :cascade do |t|
    t.integer  "category_id", null: false
    t.string   "locale",      null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
  end

  add_index "category_translations", ["category_id"], name: "index_category_translations_on_category_id", using: :btree
  add_index "category_translations", ["locale"], name: "index_category_translations_on_locale", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "document_id"
    t.integer  "account_id"
    t.text     "body"
    t.inet     "sender_ip"
    t.string   "sender_hostname"
    t.integer  "response_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "comments", ["account_id"], name: "index_comments_on_account_id", using: :btree
  add_index "comments", ["document_id"], name: "index_comments_on_document_id", using: :btree
  add_index "comments", ["response_id"], name: "index_comments_on_response_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.string   "tags"
    t.integer  "category_id"
    t.integer  "uploader_id"
    t.integer  "lang_id"
    t.integer  "views",              default: 0,   null: false
    t.decimal  "rating",             default: 0.0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "documents", ["category_id"], name: "index_documents_on_category_id", using: :btree
  add_index "documents", ["lang_id"], name: "index_documents_on_lang_id", using: :btree
  add_index "documents", ["slug"], name: "index_documents_on_slug", unique: true, using: :btree
  add_index "documents", ["uploader_id"], name: "index_documents_on_uploader_id", using: :btree

  create_table "downloads", force: :cascade do |t|
    t.string   "download_file_name"
    t.string   "download_content_type"
    t.integer  "download_file_size"
    t.datetime "download_updated_at"
    t.integer  "document_id"
    t.integer  "download_counter",      default: 0, null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "downloads", ["document_id"], name: "index_downloads_on_document_id", using: :btree

  create_table "langs", force: :cascade do |t|
    t.string   "name"
    t.string   "code",       limit: 2
    t.string   "icon_path"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "langs", ["code"], name: "index_langs_on_code", unique: true, using: :btree
  add_index "langs", ["icon_path"], name: "index_langs_on_icon_path", unique: true, using: :btree

  create_table "tokens", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "token"
    t.boolean  "reset",        default: false, null: false
    t.boolean  "confirmation", default: false, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "tokens", ["account_id"], name: "index_tokens_on_account_id", using: :btree

  add_foreign_key "categories", "categories", column: "category_parent_id", name: "category_parent_id"
  add_foreign_key "comments", "accounts"
  add_foreign_key "comments", "comments", column: "response_id", name: "response_id"
  add_foreign_key "comments", "documents"
  add_foreign_key "documents", "accounts", column: "uploader_id", name: "uploader_id"
  add_foreign_key "documents", "categories"
  add_foreign_key "documents", "langs"
  add_foreign_key "downloads", "documents"
  add_foreign_key "tokens", "accounts"
end
