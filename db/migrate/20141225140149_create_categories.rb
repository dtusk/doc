class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.references :category_parent
    end
    add_index :categories, :slug, unique: true
    add_foreign_key :categories, :categories, name: 'category_parent_id', column: 'category_parent_id'
  end
end
