class Download < ActiveRecord::Base
  belongs_to :document

  has_attached_file :download

  validates_attachment_content_type :download, content_type: /\Aapplication\/pdf\Z/
end
