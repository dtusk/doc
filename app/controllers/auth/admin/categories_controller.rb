class Auth::Admin::CategoriesController < ApplicationController

  before_filter :auth_admin_account
  before_filter :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all
  end

  def show
    @category = Category.friendly.find(params[:id])
  end

  def new
    @categories = Category.all
  end

  def create
    @category = Category.new(category_params)
    respond_to do |f|
      if @category.save
        expire_fragment 'amount_of_categories_cache'
        f.html { redirect_to auth_admin_category_path(@category), notice: I18n.t(:category_was_successfully_created) }
        f.json { render json: @category, status: :created, location: @category }
      else
        f.html { render :new }
        f.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @categories = Category.all
  end

  def update
    respond_to do |f|
      if @category.update_attributes(category_params)
        f.html { redirect_to auth_admin_category_path(@category), notice: I18n.t(:category_was_successfully_updated) }
        f.json { head :no_content }
      else
        f.html { render :new }
        f.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @category.destroy

    respond_to do |f|
      f.html { redirect_to auth_admin_categories_path }
      f.json { head :no_content }
    end
  end

  private
    def set_category
      @category = Category.friendly.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :category_parent_id)
    end
end
