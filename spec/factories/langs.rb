FactoryGirl.define do
  factory :lang do
    name { Faker::Address.country }
    code { Faker::Address.state_abbr }
    icon_path { Faker::Company.logo }
    documents
  end

end
