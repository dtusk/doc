FactoryGirl.define do
  factory :category do
    name { Faker::Lorem.sentence }
    slug { Faker::Internet.slug }
    association :category_parent, factory: :category
  end

end
