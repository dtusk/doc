class RegisterMailer < ApplicationMailer
  def activate_account(account, confirmation_token)
    @account = account
    @confirmation_token = confirmation_token

    mail(to: @account.email, subject: I18n.t('welcome_to_our_site'))
  end

  def forgot_password(account, reset_token)
    @account = account
    @reset_token = reset_token
    
    mail(to: @account.email, subject: I18n.t('your_reset_token'))
  end
end
