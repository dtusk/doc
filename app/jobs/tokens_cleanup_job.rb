class TokensCleanupJob < ActiveJob::Base

  queue_as :low_priority

  def perform(record)
    Token.find_by(token: record).destroy if Token.exists?(token: record)
  end
end
