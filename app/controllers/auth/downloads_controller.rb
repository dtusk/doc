class Auth::DownloadsController < ApplicationController

  before_filter :auth_account
  def create
    document = Document.where(slug: download_params[:document_id], uploader: current_account).limit(1).take
    redirect_to root_path, error: I18n.t(:no_document_found) unless document
    redirect_to root_path, error: I18n.t(:document_already_created) if document.downloads
    @download = document.downloads.create(download: download_params[:download])
  end

  def destroy
    document = Document.where(id: params[:document_id], uploader: current_account).limit(1).take
    redirect_to root_path, error: I18n.t(:no_document_found) unless document
    document.downloads.find_by(id: params[:id]).destroy
    render js: 'destroy.js.erb'
  end
  private
    def download_params
      params.require(:downloads).permit(:download, :document_id)
    end
end
