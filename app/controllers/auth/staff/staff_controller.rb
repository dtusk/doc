class Auth::Staff::StaffController < ApplicationController 

  before_filter :auth_staff_account

  def index
  end

  def ban_account
    if (account = Account.find(params[:id]))
      if (account.banned?)
        account.banned = false
      else
        account.banned = true
      end
      account.save
    end
    redirect_to request.referrer
  end
end
