class Document < ActiveRecord::Base
  searchkick callbacks: :async, autocomplete: [:title], highlight: {tag: "<strong>"}
  extend FriendlyId

  belongs_to :category
  belongs_to :uploader, class_name: 'Account'
  belongs_to :lang

  validates :title, presence: true
  validates :tags, presence: true

  has_many :downloads, dependent: :destroy
  has_many :comments, dependent: :destroy

  accepts_nested_attributes_for :downloads, allow_destroy: true

  friendly_id :title, use: [:slugged, :finders]
  has_attached_file :image

  def self.title_is(title)
    sensitive = where(:title => title.strip).limit(1)
    return sensitive unless sensitive.empty?
    where("UPPER(title) = UPPER(?)", title.strip).take
  end

  def to_param
    slug
  end

  def to_s
    title
  end

end
