class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.string :tags
      t.references :category, index: true
      t.references :uploader, index: true
      t.references :lang, index: true
      t.integer :views, null: false, default: 0
      t.decimal :rating, null: false, default: 0.0, limit: 2

      t.timestamps null: false
    end
    add_index :documents, :slug, unique: true
    add_foreign_key :documents, :categories
    add_foreign_key :documents, :accounts, name: 'uploader_id', column: 'uploader_id'
    add_foreign_key :documents, :langs
  end
end
